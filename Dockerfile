FROM php:7.3-fpm-alpine

# install libraries
RUN apk upgrade --update \
    && apk add \
       git \
       zlib-dev \
       libzip-dev \
       libsodium \
       libzip \
       libssh2 \
       nginx \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install pdo_mysql zip \
    && mkdir /run/nginx

RUN apk add --no-cache \
        libpng \
        libpng-dev \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libwebp-dev \
      && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-webp-dir=/usr/include/  --with-jpeg-dir=/usr/include/ \
      && docker-php-ext-install gd \
      && apk del freetype-dev libpng-dev libjpeg-turbo-dev

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
  && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
  && php -r "unlink('composer-setup.php');"

ENV COMPOSER_ALLOW_SUPERUSER 1

# setting up apps
COPY ./conf/nginx.conf /etc/nginx/nginx.conf
WORKDIR /var/www/lftvweb

FROM nginx:1.15.8-alpine

COPY ./conf/nginx.conf /etc/nginx/nginx.conf
COPY lftvweb /var/www/lftvweb

COPY ./run.sh /usr/local/bin/run.sh

COPY --from=0 /usr/local/bin/php /usr/local/bin/php
COPY --from=0 /usr/local/sbin/php-fpm /usr/local/sbin/php-fpm
COPY --from=0 /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=0 /usr/lib/libedit.so.0 /usr/lib/libedit.so.0
COPY --from=0 /lib/libz.so.1 /lib/libz.so.1
COPY --from=0 /usr/lib/libxml2.so.2 /usr/lib/libxml2.so.2
COPY --from=0 /lib/libssl.so.1.1 /lib/libssl.so.1.1
COPY --from=0 /lib/libcrypto.so.1.1 /lib/libcrypto.so.1.1
COPY --from=0 /usr/lib/libcurl.so.4 /usr/lib/libcurl.so.4
COPY --from=0 /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=0 /usr/lib/libncursesw.so.6 /usr/lib/libncursesw.so.6
COPY --from=0 /usr/lib/libssh2.so.1 /usr/lib/libssh2.so.1
COPY --from=0 /usr/lib/libargon2.so.1 /usr/lib/libargon2.so.1
COPY --from=0 /usr/lib/libnghttp2.so.14 /usr/lib/libnghttp2.so.14
COPY --from=0 /usr/lib/libsodium.so.23 /usr/lib/libsodium.so.23
COPY --from=0 /usr/lib/libzip.so.5 /usr/lib/libzip.so.5

COPY --from=0 /usr/local/etc /usr/local/etc
COPY --from=0 /usr/local/lib/php/extensions /usr/local/lib/php/extensions
COPY --from=0 /usr/local/bin/composer /usr/local/bin/composer

WORKDIR /var/www/lftvweb

RUN chown nginx:nginx storage/logs \
    && sed -i 's/www-data/nginx/g' /usr/local/etc/php-fpm.d/www.conf \
    && chown -R nginx:nginx storage/framework \
    && ln -s /var/www/lftvweb/public /usr/share/nginx/html

CMD ["run.sh"]
