<?php

return [
    'recruits' => [
        'positions' => [
            5  => 'BrSE',
            6  => 'Project_Manager',
            16 => 'Team_Leader',
            4  => 'Junior_Developer',
            15 => 'Senior_Developer',
            18 => 'Infra_Engineer',
            19 => 'Hr',
            20 => 'Accountant'
        ]
    ],
    'messages' => [
        'mail_sent_successful' => 'Thank you for sending us your CV. We will contact soon.'
    ],
    'languages' => [
        'en' => 'English',
        'jp' => '日本語'
    ],
    'admin' => [
        'max_item_on_page' => 15,
    ]
];