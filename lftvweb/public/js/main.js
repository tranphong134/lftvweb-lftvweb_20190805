jQuery(document).ready(function() {
  $('#imageContainer').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    imgSrc = button.find('.img-thumbnail').attr('src');
    var imgTag = '<img class="img-fluid" src="' + imgSrc + '" />';
    var modal = $(this);
    modal.find('.modal-body').html(imgTag);
  })

  // ===== Scroll to Top =====
  $(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {      // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
      $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
  });

  $('#return-to-top').click(function() {
    $('body,html').animate({
        scrollTop : 0
    }, 500);
  });
});