<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Utils
{
    /**
     * Get array of File in specified directory
     * @return array SplFileInfo
    */
    public static function getAllImagesInDir($dirName) {
        $files = File::files(storage_path().'/app/public/images/'.$dirName);
        $util = new Utils();
        $util->createCoverImage($files);
        return $files;
    }

    /**
     * Resize parent image and create cover image directory
     *
     * @param Array SplFileInfo $files
     */
    private function createCoverImage($files) {
        $galleryPath = Config::get('filesystems.disks.public.root') . '/images/gallery/';
        $coversDir = $galleryPath . 'covers/';

        //create cover images from original image
        foreach ($files as $file) {
            $fileName = $file->getFileName();
            if(File::exists($coversDir . $fileName))
                continue;
            Image::make($galleryPath . $fileName)->resize(100, 67)->save($coversDir . $fileName);
        }
    }
}
