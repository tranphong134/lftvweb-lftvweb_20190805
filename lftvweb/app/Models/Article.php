<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use \Encore\Admin\Traits\Resizable;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'article';
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    protected $fillable = ['type', 'image', 'title', 'content', 'summary', 'is_published',
        'is_always_top', 'date_created', 'date_modified', 'deleted_at', 'read_counter', 'published_at'];
    public $timestamps = false;

    /* Relation */
    public function category() {
        return $this->belongsTo('App\Models\Category', 'type');
    }

    /**
     * Methods for Listing page
     */
    public static function getArticleForListing($type, $limit = 9, $is_paging = false) {
        $q = Article::with('category')
            ->where('type', $type)
            ->notDeleted()
            ->isPublished();
        if ($is_paging)
            $q->paginate($limit);
        else
            $q->take($limit);
        return $q->get();
    }

    public static function getAboutArticles() {
        $ids = [ABOUT_US, OUR_VISION, GUIDELINES, OFFSHORE_SERVICE, LAB_SERVICE, OUR_SERVICE];
        $articles = Article::with('category')
            ->whereIn('id', $ids)
            ->notDeleted()
            ->isPublished()
            ->get();

        $res = [];
        foreach($articles as $article) {
            $res[$article->id] = $article;
        }
        return $res;
    }

    /**
     * Methods for Detail page
     */
    public static function getArticleDetail($id) {
        if (!is_array($id)) {
            $id = [$id];
        }
        return Article::with('category')
            ->where('id', $id)
            ->notDeleted()
            ->isPublished()
            ->get();
    }

    public static function increaseReadCounterByOne($id) {
        return Article::where('id', $id)
            ->increment('read_counter', 1);
    }

    public static function getMostReadArticle($category_id, $limit = 3) {
        return Article::with('category')
            ->where('type', $category_id)
            ->notDeleted()
            ->isPublished()
            ->take($limit)
            ->orderBy('read_counter', 'DESC')
            ->get();
    }

    /**
     * Scope methods
     */
    public function scopeNotDeleted($q) {
        return $q->where('deleted_at', NULL);
    }

    public function scopeIsPublished($q) {
        return $q->where('is_published', 1);
    }

    public function scopeNewest($q) {
        return $q->orderBy('date_created', 'DESC');
    }
}
