<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    protected $table = 'company_info';
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    protected $fillable = ['company_name', 'phone', 'email', 'address', 'bussiness', 'representative', 'number_of_staffs', 
        'facebook_page_id', 'facebook_access_token', 'logo_image', 'number_of_clients', 'number_of_projects',
        'google_map_embed_url', 'google_map_width', 'google_map_height'];
    public $timestamps = false;

    public static function getDetails() {
        return CompanyInfo::find(1);
    }
}
