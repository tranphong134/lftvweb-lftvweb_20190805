<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use \Encore\Admin\Traits\Resizable;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    protected $fillable = ['name', 'date_created', 'date_modified', 'is_enabled', 'deleted_at', 'is_allowed_on_front',
        'image', 'level', 'parent', 'frontend_uri'];
    public $timestamps = false;

    /**
     * Get data for navigation menu
     */
    public static function getFrontendItem() {
        return Category::notDeleted()
            ->isFrontend()
            ->is1stLevel()
            ->orderBy('id', 'DESC')
            ->get();
    }

    public static function getForSelectBox() {
        $categories = Category::notDeleted()->get();
        foreach($categories as $category){
            $res[$category->id] = $category->name;
        }
        return $res;
    }

    /**
     * Scope methods
     */
    public function scopeNotDeleted($q) {
        return $q->where('deleted_at', NULL);
    }

    public function scopeIsFrontend($q) {
        return $q->where('is_allowed_on_front', 1);
    }

    public function scopeIs1stLevel($q) {
        return $q->where('level', 1);
    }
}
