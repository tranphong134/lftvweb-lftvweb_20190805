<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactRequest extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'contact_request';
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    protected $fillable = ['phone_vn', 'phone_jp', 'sender_email', 'message', 'reply', 
        'date_created', 'date_replied', 'is_replied', 'deleted_at'];
    public $timestamps = false;

    public static function getDetails() {
        return CompanyInfo::find(1);
    }
}
