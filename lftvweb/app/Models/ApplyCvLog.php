<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplyCvLog extends Model
{
    protected $table = 'apply_cv_log';
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    protected $fillable = ['applied_at', 'applicant_name', 'applicant_email', 
        'applicant_phone', 'applicant_message', 'applicant_cv_filename'];
    public $timestamps = false;
}
