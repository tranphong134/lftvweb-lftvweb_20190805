<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;

class SitemapController extends Controller
{
    public function index()
    {
      $articles = Article::all()->first();
      $categories = Category::all()->first();

      return response()->view('sitemap.index', [
          'articles' => $articles,
          'categories' => $categories
      ])->header('Content-Type', 'text/xml');
    }

    public function articles()
    {
        $articles = Article::newest()->get();
        return response()->view('sitemap.article', [
            'articles' => $articles,
        ])->header('Content-Type', 'text/xml');
    }

    public function categories()
    {
        $categories = Category::all();
        return response()->view('sitemap.category', [
            'categories' => $categories,
        ])->header('Content-Type', 'text/xml');
    }
}
