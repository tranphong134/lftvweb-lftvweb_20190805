<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Helpers\Utils;

class TopController extends BaseFrontendController
{
    public function index ()
    {
        $this->prepareCompanyInfo();
        return view('top', array(
            'abouts' => Article::getAboutArticles(),
            'recruits' => Article::getArticleForListing(RECRUIT),
            'gallery' => Utils::getAllImagesInDir('gallery')
        ));
    }
}
