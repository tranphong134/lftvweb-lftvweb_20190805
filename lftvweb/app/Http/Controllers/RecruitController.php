<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Http\Requests\ApplyCvFormRequest;
use App\Mail\ApplyCvMail;
use App\Models\ApplyCvLog;
use App\Models\CompanyInfo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class RecruitController extends BaseFrontendController
{
    /**
     * Show the detail for the given job name.
     *
     * @param String  $lang
     * @param String  $name
     * @return View
     */
    public function show($lang, $id)
    {
        $article = Article::getArticleDetail($id);
        $this->prepareCompanyInfo();
        return view('recruit-detail', [
            'pageName' => $article[0]->title,
            'article' => $article[0],
            'bannerImage' => $article[0]->category->image
        ]);
    }

    public function send(ApplyCvFormRequest $request) {
        $file = $request->file('file');

         // generate a new filename. getClientOriginalExtension() for the file extension
        $filename =  Config::get('constants.recruits.positions')[$request->get('job_position')]. '-' . Carbon::now()->format('ymdHi') . '.' . $file->getClientOriginalExtension();
        // save to storage/app/photos as the new $filename
        $path = $file->storeAs('public/cvs', $filename);

        $applyInfo = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'message' => empty($request->get('message'))? '' : $request->get('message'),
            'attachment' => $path,
            'filename' => $filename
        ];
        $this->saveToLog($applyInfo);

        if($result = $this->sendEmailToHr($applyInfo))
            return redirect(app()->getLocale() . '/recruit/'.$request->get('job_position'))->with('status', Config::get('constants.messages.mail_sent_successful'));
        else
            return redirect(app()->getLocale() . '/recruit/'.$request->get('job_position'))->with('errors', $result);
    }

    /**
     * Send mail to Admin mail box
     *
     * @param Array $applyInfo
     * @return Boolean
     */
    private function sendEmailToHr($applyInfo)
    {
        $companyInfo = CompanyInfo::getDetails();
        try {
            Mail::to($companyInfo['email'])
                ->queue(new ApplyCvMail($applyInfo));
            return true;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    private function saveToLog($applyInfo)
    {
        $log = new ApplyCvLog();
        $log->applicant_name = $applyInfo['name'];
        $log->applicant_phone = $applyInfo['phone'];
        $log->applicant_email = $applyInfo['email'];
        $log->applicant_message = $applyInfo['message'];
        $log->applicant_cv_filename = $applyInfo['filename'];
        $log->save();
    }
}
