<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;

class NewsController extends BaseFrontendController
{
    public function index ()
    {
        $news = Article::getArticleForListing(NEWS, 5);
        $this->prepareCompanyInfo();
        return view('news', [
            'news' => $news,
            'bannerImage' => $news[0]->category->image
        ]);
    }

    /**
     * Show the detail for the given job name.
     *
     * @param  string  $name
     * @return View
     */
    public function show($id)
    {
        $article = Article::getArticleDetail($id);
        $this->prepareCompanyInfo();
        Article::increaseReadCounterByOne($id);
        return view('news-detail', [
            'pageName' => $article[0]->title,
            'article' => $article[0],
            'categories' => Category::getFrontendItem(),
            'mostReadArticles' => Article::getMostReadArticle(NEWS),
            'bannerImage' => $article[0]->category->image
        ]);
    }
}
