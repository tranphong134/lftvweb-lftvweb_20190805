<?php

namespace App\Http\Controllers;

use App\Models\Article;

class AboutController extends BaseFrontendController
{
    public function index ()
    {
        $articles = Article::getAboutArticles();
        $this->prepareCompanyInfo();
        return view('about', [
            'bannerImage' => $articles[ABOUT_US]->category->image,
            'articles' => $articles
        ]);
    }
}
