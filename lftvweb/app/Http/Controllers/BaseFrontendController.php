<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\View as IlluminateView;
use App\Models\CompanyInfo;
use Illuminate\Support\Facades\Route;

class BaseFrontendController extends Controller
{
    protected function prepareCompanyInfo() {
        $companyInfo = CompanyInfo::getDetails();
        IlluminateView::share('companyInfo', $companyInfo);

        $categories = Category::getFrontendItem();
        IlluminateView::share('navigationMnItems', $categories);

        $route = Route::current();
        IlluminateView::share('activeNavItem', $route->uri);
    }
}
