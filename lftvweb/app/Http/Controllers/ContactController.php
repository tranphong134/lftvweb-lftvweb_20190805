<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Models\Category;
use App\Models\ContactRequest;

class ContactController extends BaseFrontendController
{
    public function index() {
        $this->prepareCompanyInfo();
        $category = Category::find(CONTACT);
        return view('contact', [
            'bannerImage' => $category->image
        ]);
    }

    public function store(ContactFormRequest $request) {
        $contact = new ContactRequest();
        $contact->sender_email = $request->get('email');
        $contact->phone_vn = $request->get('phone_vn');
        $contact->phone_jp = $request->get('phone_jp');
        $contact->message = $request->get('message');

        $contact->save();
        return redirect()->route('contact.index')->with('status', SENT_SUCCESS_MESSAGE);
    }
}
