<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'email'        => 'required|email',
            'phone_vn'     => 'required|numeric',
            'phone_jp'     => 'nullable',
            'message'      => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'Enter Name',
            'email.required'    => 'Enter Email',
            'email.email'       => 'Enter a valid Email',
            'phone_vn.required' => 'Enter Vietnam phone',
            'phone_jp.numeric'  => 'Enter a valid phone number',
            'message.required'  => 'Enter Message',
        ];
    }
}
