<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplyCvFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'email'        => 'required|email',
            'phone'        => 'required|digits:10',
            'message'      => '',
            'file'         => 'required|max:10000|mimes:pdf',
            'job_position' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'Enter Name',
            'email.required'    => 'Enter Email',
            'email.email'       => 'Enter a valid Email',
            'phone.required'    => 'Enter phone',
            'message.required'  => 'Enter Message',
        ];
    }
}
