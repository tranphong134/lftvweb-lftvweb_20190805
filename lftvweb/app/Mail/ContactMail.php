<?php

namespace App\Mail;

use App\Models\ContactRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Email signature
     */
    protected $mailSignature;

    /**
     * Request form array
     */
    protected $contactRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contactRequest, $mailSignature = '')
    {
        $this->contactRequest = $contactRequest;
        $this->mailSignature = $mailSignature;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reply for request on '.$this->contactRequest['date_created'])
            ->view('email-layout')
            ->with([
                'title' => 'REPLIED EMAIL',
                'quoteMessage' => $this->contactRequest['message'],
                'reply' => $this->contactRequest['reply'],
                'signature' => $this->mailSignature
            ]);
    }
}
