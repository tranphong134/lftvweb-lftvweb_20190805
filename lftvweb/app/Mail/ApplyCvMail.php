<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplyCvMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Request form array
     */
    protected $applyInfo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($applyInfo)
    {
        $this->applyInfo = $applyInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Applicants for ' . $this->applyInfo['filename'])
            ->view('recruits.cv_mail')
            ->attach(storage_path() . '/app/' . $this->applyInfo['attachment'])
            ->with([
                'candidate_name' => $this->applyInfo['name'],
                'candidate_email' => $this->applyInfo['email'],
                'candidate_phone' => $this->applyInfo['phone'],
                'candidate_message' => $this->applyInfo['message']
            ]);
    }
}
