<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Controllers\AdminController;
use App\Models\CompanyInfo;

class CompanyInfoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Company Info management';

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id=null)
    {
        $form = new Form(new CompanyInfo());
        $form->text('company_name', __('Company name'))->rules('required');
        $form->text('phone', __('Phone'))->rules('required');
        $form->text('email')->rules('required|email');
        $form->text('address')->rules('required|max:100');
        $form->text('representative')->rules('required|max:50');
        $form->text('facebook_page_id');
        $form->textarea('facebook_access_token')->rules('nullable');
        $form->image('logo_image_top')->thumbnail('small')->rules('required|image');
        $form->image('logo_image_footer')->thumbnail('small|image');
        $form->text('number_of_staffs')->rules('required');
        $form->text('number_of_clients')->rules('required');
        $form->text('number_of_projects')->rules('required');
        $form->textarea('google_map_embed_url')->rules('required');
        $form->text('google_map_width')->rules('required');
        $form->text('google_map_height')->rules('required');

        $form->saved(function (Form $form) {
            $id = $form->model()->id;
            return redirect('/admin/company-info/'.$id.'/edit');
        });

        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();

        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
            $tools->disableList();
        });

        return $form;
    }
}
