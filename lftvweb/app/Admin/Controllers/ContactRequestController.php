<?php

namespace App\Admin\Controllers;

use App\Mail\ContactMail;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Mail;
use App\Models\ContactRequest;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class ContactRequestController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Contact Request management';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ContactRequest);

        $grid->model()->orderBy('date_created', 'DESC');
        $grid->column('id', __('ID'))->sortable();
        $grid->column('phone_vn', __('VN phone'));
        $grid->column('phone_jp', __('JP phone'));
        $grid->column('sender_email', __('Author'));
        $grid->column('is_replied')->display(function(){
            return GRID_STATUS[$this->is_replied];
        });
        $grid->column('date_replied');
        $grid->column('date_created');
        $grid->paginate(Config::get('constants.admin.max_item_on_page'));

        $grid->disableCreateButton();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
        });

        $grid->filter(function($filter){
            $filter->like('title', 'Title');
            $filter->between('date_created', 'Created date')->date();
            $filter->equal('is_replied')->radio([
                0 => 'No',
                1 => 'Yes'
            ]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ContactRequest);

        $form->display('id', __('ID'));
        $form->mobile('phone_vn', __('VN phone'))->rules('required|phone');
        $form->mobile('phone_jp', __('JP name'))->rules('phone');
        $form->email('sender_email', __('Author'))->rules('required|email');
        $form->display('message');
        $form->ckeditor('reply')->rules('required');
        $form->radio('is_replied')->options([0 => STATUS[DISABLED], 1 => STATUS[ENABLED]])->default(0);
        $form->submitted(function (Form $form) {
            $contactFormRequest = [
                'date_created' => $form->model()->date_created,
                'message' => $form->model()->message,
                'reply' => request('reply')
            ];

            if(request('is_replied')) {
                $message = $this->addFeedback($contactFormRequest);
                if($message)
                    admin_success('Success message', 'Send reply email successfully!');
                else
                    admin_error('Error message', $message['error']);
            }
        });
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });

        return $form;
    }

    private function addFeedback($contactFormRequest)
    {
        try {
            Mail::to(env('MAIL_USERNAME'))
                ->queue(new ContactMail($contactFormRequest));
            return true;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

}
