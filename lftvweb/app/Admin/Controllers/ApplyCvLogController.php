<?php

namespace App\Admin\Controllers;

use App\Models\ApplyCvLog;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Config;

class ApplyCvLogController extends AdminController
{
     /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Category management';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ApplyCvLog());
        $grid->model()->orderBy('applied_at', 'DESC');
        $grid->column('applied_at');
        $grid->column('applicant_name');
        $grid->column('applicant_phone');
        $grid->column('applicant_email');
        $grid->column('applicant_cv_filename');
        $grid->paginate(Config::get('constants.admin.max_item_on_page'));

        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableDelete();
            $actions->disableEdit();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
    }
}
