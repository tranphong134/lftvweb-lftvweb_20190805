<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class ArticleController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Article management';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Article);
        $grid->model()->orderBy('date_created', 'DESC');
        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', __('EN Title name'));
        $grid->column('jp_title', __('JP Title name'));
        $grid->column('type', __('Category'))->display(function($type){
            $category = Category::find($type);
            return $category->name;
        });
        $grid->column('image')->display(function(){
            return $this->thumbnail('small');
        })->image();
        $grid->column('is_always_top', __('Pin to top'))->display(function () {
            return GRID_STATUS[$this->is_always_top];
        });
        $grid->column('published_at', __('Published at'));
        $grid->column('date_created', __('Created at'));
        $grid->column('date_modified', __('Updated at'));
        $grid->paginate(Config::get('constants.admin.max_item_on_page'));

        $grid->filter(function($filter){
            $filter->like('title', 'Title');
            $filter->equal('type', 'Category')->select(function(){
                return Category::getForSelectBox();
            })->default(0);
            $filter->between('date_created', 'Created date')->date();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Article::findOrFail($id));

        $show->field('id', __('ID'));
        $show->type()->as(function($id){
            $category = Category::find($id);
            return $category->name;
        });
        $show->field('EN title', __('EN Title'));
        $show->field('JP title', __('JP Title'));
        $show->field('summary', __('Summary'));
        $show->field('content', __('Contents'));
        $show->field('image')->display(function(){
            return $this->thumbnail('small');
        })->image();
        $show->is_always_top()->as(function () {
            return GRID_STATUS[$this->is_always_top];
        });
        $show->field('date_created', __('Created at'));
        $show->field('date_modified', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Article);

        $form->display('id', __('ID'));
        $form->text('title', __('Title'))->rules('required');
        $form->text('jp_title', __('JP Title'))->rules('required');
        $form->select('type', __('Category'))->options(Category::all()->pluck('name', 'id'))->rules('required');
        $form->ckeditor('summary')->rules('required');
        $form->ckeditor('content')->rules('required');
        $form->ckeditor('jp_content');
        $form->image('image')->thumbnail('small', 350, 200)->uniqueName()->rules('image');
        $form->radio('is_published', __('Publish'))->options([0 => STATUS[DISABLED], 1 => STATUS[ENABLED]])->default(1);
        $form->radio('is_always_top', __('Pin to top'))->options([0 => STATUS[DISABLED], 1 => STATUS[ENABLED]])->default(1);

        $form->saving(function (Form $form) {
            if($form->is_published) {
                $form->model()->published_at = Carbon::now()->toDateTimeString();
            }
        });

        return $form;
    }
}
