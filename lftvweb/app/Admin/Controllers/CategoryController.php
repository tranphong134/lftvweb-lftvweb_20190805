<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Category;

class CategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Category management';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);
        $grid->model()->orderBy('parent');
        $grid->column('id', __('ID'))->sortable();
        $grid->column('name')->display(function(){
            $data = $this->getAttributes();
            if($data['level'] == '2')
                return '--'.$this->name;
            elseif($data['level'] == '3')
                return '----'.$this->name;
            return $this->name;
        });
        $grid->column('jp_name')->display(function(){
            $data = $this->getAttributes();
            if($data['level'] == '2')
                return '--'.$this->jp_name;
            elseif($data['level'] == '3')
                return '----'.$this->jp_name;
            return $this->jp_name;
        });
        $grid->column('image')->display(function(){
            return $this->thumbnail('small');
        })->image();
        $grid->column('is_enabled', __('Is enabled'))->display(function () {
            return GRID_STATUS[$this->is_enabled];
        });
        $grid->column('is_allowed_on_front', __('Used In Front end'))->display(function () {
            return GRID_STATUS[$this->is_allowed_on_front];
        });
        $grid->column('frontend_uri', __('Frontend Uri'));
        $grid->column('date_created', __('Created at'));
        $grid->column('date_modified', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name');
        $show->field('jp_name');
        $show->field('image')->thumbnail('small')->image();
        $show->is_enabled()->as(function () {
            return GRID_STATUS[$this->is_enabled];
        });
        $show->is_allowed_on_front()->as(function () {
            return GRID_STATUS[$this->is_allowed_on_front];
        });
        $show->field('level');
        $show->field('frontend_uri', __('Frontend Uri'));
        $show->field('date_created', __('Created at'));
        $show->field('date_modified', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->display('id', __('ID'));
        $form->select('parent', __('Parent'))->options(function(){
            return Category::getForSelectBox();
        })->rules('nullable');
        $form->text('name')->rules('required');
        $form->text('jp_name')->rules('required');
        $form->image('image')->thumbnail('small', $width = 350, $height = 250)->uniqueName()->rules('required|image');
        $form->radio('is_enabled', __('Status'))->options([0 => 'Disable', 1 => 'Enable'])->default(1);
        $form->radio('is_allowed_on_front', __('Used In Front end'))->options([0 => 'No', 1 => 'Yes'])->default(1);
        $form->text('frontend_uri', __('Frontend Uri'))->rules('required');

        $form->saving(function (Form $form) {
            if($form->parent > 0) {
                $parentCategory = Category::find($form->parent);
                $form->model()->level = $parentCategory->level + 1;
            }
        });

        return $form;
    }
}
