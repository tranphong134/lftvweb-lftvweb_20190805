<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('category', CategoryController::class);
    $router->resource('article', ArticleController::class);
    $router->resource('company-info', CompanyInfoController::class);
    $router->resource('contact-request', ContactRequestController::class);
    $router->resource('apply-cv-log',ApplyCvLogController::class);
});
