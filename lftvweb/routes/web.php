<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    switch (substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2))
    {
        default:
            $locale = 'en';
            break;
        // uncomment this when using multi-language
        // case 'ja':
        //     $locale ='jp';
        //     break;
    }
    return redirect($locale);
});

Route::group([
    'prefix' => '{locale}',
    // 'where' => ['locale' => '[a-zA-Z]{2}'],  // uncomment this when using multi-language
    'where' => ['locale' => 'en'],              // remove this when using multi-language
    'middleware' => 'setlocale'
], function() {

    Route::get('/', 'TopController@index');

    /**
     * RECRUITS
     */
    Route::get('/recruit/{name}', 'RecruitController@show')->name('recruit.show');
    Route::post('/recruit/send', 'RecruitController@send')->name('recruit.send');

    /**
     * SITEMAP
     */
    Route::get('/sitemap.xml', 'SitemapController@index');
    Route::get('/sitemap.xml/articles', 'SitemapController@articles');
    Route::get('/sitemap.xml/categories', 'SitemapController@categories');

    Auth::routes();
});

/**
 * URL query string that doesn’t match anything above this – will redirect to homepage
 */
Route::any('{query}', function() {
    return redirect('/');
})->where('query', '.*');