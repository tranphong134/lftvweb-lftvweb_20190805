<html>
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ URL::asset('css/imagehover.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/lftv-main.css') }}">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <script src="https://kit.fontawesome.com/17371a4478.js"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </head>
    <body>
        <!-- Header -->
        <header>
        @include('_navigation', ['navbarId' => 'navbarResponsive'])
        </header>

        <!-- Content -->
        <div class="container-fluid p-0">
            @yield('content')
        </div>

        <!-- Footer -->
        <footer class="py-2 bg-lifull">
            @include('_footer')
        </footer>
    </body>
</html>