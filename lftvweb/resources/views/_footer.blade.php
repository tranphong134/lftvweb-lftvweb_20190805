<a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up"></i></a>
<div class="container">
    <div class="row text-white">
        <div class="col-md-5 footer_logo">
            <a class="navbar-brand mx-auto" href="/">
                <img class="logo-footer" atl="LFTV Logo" src="{{ URL::asset('/storage/'.$companyInfo->logo_image_footer) }}"/>
            </a>
            <p class="m-0">Copyright &copy; LIFULL Tech Vietnam Co.,Ltd. All Rights Reserved.</p>
        </div>
        {{-- Uncomment this when using multi-language --}}
        {{-- <div class="col-md-3 col-9 language_menu">
            <p class="my-0">@lang('locale.change_language_to')&nbsp;
            @foreach (Config::get('constants.languages') as $key => $val)
                @if ($key != app()->getLocale())
                    <span><a class="section_title text-white-color" href="/{{ $key }}">{{ $val }}</a></span>
                @endif
            @endforeach
            </p>
        </div> --}}
        <div class="col-md-1 offset-md-3 d-flex align-items-center">
            <p class="mb-0 mx-auto"><a target="_blank" href="https://www.facebook.com/LIFULLTechVN/" ><img width="40px" src="/images/fb_logo.png" /></a></p>
        </div>
        <div class="col-md-3">
            <p class="mb-0 mt-2">{{ $companyInfo->address }}</p>
            <p class="mb-0">@lang('locale.phone'): {{ $companyInfo->phone }}</p>
        </div>
    </div>
</div>
