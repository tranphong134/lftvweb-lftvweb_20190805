<h5 class="card-title">{{ $title }}</h5>
<blockquote class="blockquote">
    <p>{{ $quoteMessage }}</p>
</blockquote>
{!! $reply !!}
<p>{{ $signature }}</p>