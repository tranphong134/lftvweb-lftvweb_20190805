<div class="container mt-5">
    <hr class="section_border_bottom">
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('status'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {!! Session::get('status') !!}
        </div>

    @endif
    {!! Form::open(['route' => 'contact.store']) !!}
        <div class="form-row">
            <div class="form-group col-md-6">
                {!! Form::label('name', 'Your Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            
            <div class="form-group col-md-6">
                {!! Form::label('email', 'E-mail Address') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                {!! Form::label('phone_vn_lb', 'Vietnam phone') !!}
                {!! Form::text('phone_vn', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('phone_jp_lb', 'Japanese phone') !!}
                {!! Form::text('phone_jp', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('message_lb', 'Message') !!}
            {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group text-center">
            {!! Form::submit('Submit', ['class' => 'btn bg-lifull text-white-color']) !!}
        </div>
    {!! Form::close() !!}
</div>
