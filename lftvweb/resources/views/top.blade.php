@extends('_layout')

@section('title', 'LFTV::Home')

@section('content')
<div>
	<section>
		@include('tops.intro', array('pageName' => 'Home', 'logoText' => 'LIFULL Tech Vietnam'))
	</section>
	<section id="about" class="about py-5">
		@include('tops.about', ['pageName' => 'Home'])
	</section>
	<section>
		@include('tops.service', ['pageName' => 'Home'])
	</section>
	<section class="gallery py-4">
		@include('tops.gallery', ['pageName' => 'Home', 'gallery' => $gallery])
	</section>
	<section id="recruit" class="recruit">
		@include('tops.recruit', ['pageName' => 'Home'])
	</section>
</div>
</div>
@endsection
