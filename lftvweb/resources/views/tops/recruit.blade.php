<div class="p-0 text-center">
	<div class="container my-5 ">
		<div class="section-title-div mx-auto pt-1 mb-5">
			<h1 class="section_title text-lifull-color">@lang('locale.recruits')</h1>
		</div>
		@foreach ($recruits as $index => $article)
			@if ($index%3 === 0)
				<div class="row justify-content-md-center mt-2">
			@endif
			@php $locale = app()->getLocale() @endphp
			@if ($locale == 'jp')
				@php $title = $article->jp_title @endphp
			@else
				@php $title = $article->title @endphp
			@endif
			<div class="col-md-4 mb-2">
				<figure class="imghvr-shutter-in-vert">
					<img class="img-fluid mx-auto d-block mb-2 rounded" src="{{ env('APP_URL').'/storage/'.$article->thumbnail('small') }}" alt="{{ $title }}" />
					<h4 class="mx-auto section_title text-lifull-color">{{ $title }}</h4>
					<figcaption class="d-flex flex-wrap align-items-center">
						<h3 class="mx-auto section_title">{{ $title }}</h3>
						<div class='w-100'>{!! $article->summary !!}</div>
					</figcaption>
					<a href="{{ env('APP_URL') }}/{{ $locale }}/recruit/{{ $article->id }}"></a>
				</figure>
			</div>
			@if ($index%3 !== 0 && ($index+1)%3 === 0)
				</div>
			@endif
		@endforeach
	</div>
</div>