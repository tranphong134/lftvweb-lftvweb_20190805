<div class="container section-title-div mx-auto pt-1 mb-4">
    <h1 class="section_title text-lifull-color text-center">@lang('locale.gallery')</h1>
    <hr class="section_border_bottom">
</div>
<div id="gallery" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach ($gallery as $image)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <img class="img-fluid mx-auto d-block mb-3" src="{{ URL::asset('storage/images/gallery/'.$image->getFileName()) }}" alt="" />
            </div>
        @endforeach
    </div>
    <a class="carousel-control-prev d-none d-md-flex" href="#gallery" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next d-none d-md-flex" href="#gallery" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <div class="carousel-indicators mx-0 d-flex flex-nowrap">
        @foreach ($gallery as $image)
            <div data-target="#gallery" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}">
                <img class="img-fluid mx-auto d-block" src="{{ URL::asset('storage/images/gallery/covers/'.$image->getFileName()) }}" alt="" />
            </div>
        @endforeach
    </div>
</div>