<div class="container text-center">
	<h1 class="section_title text-lifull-color">@lang('locale.about_us')</h1>
	<hr class="section_border_bottom">
	<div class="row my-5">
		<div class="col-md-6">
			@include('_lifull_logo')
		</div>
		<div class="col-md-6">
			<p class="text-center mt-3">
			@if (app()->getLocale() == 'jp')
				{!! $abouts[ABOUT_US]->jp_content !!}
			@else
				{!! $abouts[ABOUT_US]->content !!}
			@endif
			</p>
		</div>
	</div>
</div>
