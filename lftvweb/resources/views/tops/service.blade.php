<div class="container">
    <div class="row my-5">
        <div class="col-md-12">
            <h3 class="section_title lifull-border-bottom text-dark-gray-color text-left mb-4">{{ (app()->getLocale() == 'jp') ? $abouts[OUR_VISION]->jp_title : $abouts[OUR_VISION]->title }}</h3>
            <div class="">{!! (app()->getLocale() == 'jp') ? $abouts[OUR_VISION]->jp_content : $abouts[OUR_VISION]->content !!}</div>
        </div>
    </div>
    <div class="row my-5 guidelines">
        <div class="col-md-12">
            <h3 class="section_title lifull-border-bottom text-dark-gray-color text-left mb-4">{{ (app()->getLocale() == 'jp') ? $abouts[GUIDELINES]->jp_title : $abouts[GUIDELINES]->title }}</h3>
            <div class="text-justify">{!! (app()->getLocale() == 'jp') ? $abouts[GUIDELINES]->jp_content : $abouts[GUIDELINES]->content !!}</div>
        </div>
    </div>
    <div class="my-5">
        <div class="row">
            <div class="col-md-12">
                <h3 class="section_title lifull-border-bottom text-dark-gray-color text-left mb-4">@lang('locale.our_services')</h3>
                <div class="text-justify">{!! (app()->getLocale() == 'jp') ? $abouts[OUR_SERVICE]->jp_content : $abouts[OUR_SERVICE]->content !!}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <img src="{{ env('APP_URL').'/storage/'.$abouts[LAB_SERVICE]->thumbnail('small') }}"
                        class="card-img-top mx-auto" alt="{!! (app()->getLocale() == 'jp') ? $abouts[LAB_SERVICE]->jp_title : $abouts[LAB_SERVICE]->title !!}">
                    <div class="card-body">
                        <h5 class="card-title text-lifull-color text-center section_title">{{ (app()->getLocale() == 'jp') ? $abouts[LAB_SERVICE]->jp_title : $abouts[LAB_SERVICE]->title }}</h5>
                        <div class="card-text text-justify">{!! (app()->getLocale() == 'jp') ? $abouts[LAB_SERVICE]->jp_content : $abouts[LAB_SERVICE]->content !!}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <img src="{{ env('APP_URL').'/storage/'.$abouts[OFFSHORE_SERVICE]->thumbnail('small') }}"
                        class="card-img-top mx-auto" alt="{{ $abouts[OFFSHORE_SERVICE]->title }}">
                    <div class="card-body">
                        <h5 class="card-title text-lifull-color text-center section_title">{{ (app()->getLocale() == 'jp') ? $abouts[OFFSHORE_SERVICE]->jp_title : $abouts[OFFSHORE_SERVICE]->title }}</h5>
                        <div class="card-text text-justify">{!! (app()->getLocale() == 'jp') ? $abouts[OFFSHORE_SERVICE]->jp_content : $abouts[OFFSHORE_SERVICE]->content !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
