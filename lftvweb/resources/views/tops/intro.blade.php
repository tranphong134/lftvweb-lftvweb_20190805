<div class="container-fluid py-5 top-header">
	<div class="ml-1 mt-5 mb-5">
		<h3 class='text-lifull-color h_small focusRing mt-5'>
			<span class="focusRing__ring">
				<span class="focusRing__ring">
				</span>
			</span>
			{{$logoText}}
		</h3>
		<p class="mb-3 mt-3 text-white-color"><i>@lang('locale.vision')</i></p>
	</div>
</div>