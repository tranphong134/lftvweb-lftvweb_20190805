@extends('_layout')

@section('title', 'LFTV::Home')

@section('content')
    @include('_header',[
        'pageName' => $pageName,
        'bannerImage' => $bannerImage
    ])
    @include('recruits.detail', ['article' => $article])
@endsection