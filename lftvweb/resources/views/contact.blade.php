@extends('_layout')

@section('title', 'LFTV::Home')

@section('content')
    @include('_header', [
        'pageName' => 'Contact to LFTV',
        'bannerImage' => $bannerImage
    ])
    @include('contacts.form')
@endsection