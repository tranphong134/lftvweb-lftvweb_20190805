<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="/{{ app()->getLocale() }}">
            <img class="logo" atl="LFTV Logo" src="{{ URL::asset('/storage/'.$companyInfo->logo_image_top) }}"/>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="{{ $navbarId }}">
            <ul class="navbar-nav ml-auto">
                @foreach ($navigationMnItems as $item)
                    <li class="nav-item">
                        <a class="nav-link" href="/{{ app()->getLocale() }}#{{ $item->frontend_uri }}">{{ (app()->getLocale() == 'jp') ? $item->jp_name : $item->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>