@extends('_layout')

@section('title', 'LFTV::Home')

@section('content')
    @include('_header', [
        'pageName' => 'Recruit',
        'bannerImage' => $bannerImage
    ])
@endsection