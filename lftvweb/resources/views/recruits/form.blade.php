@if ($errors->any())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (Session::has('status'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! Session::get('status') !!}
    </div>
@endif
{!! Form::open(['route' => ['recruit.send', 'lang' => app()->getLocale()], 'enctype' => 'multipart/form-data', 'id' => 'applyForm']) !!}
    <div class="form-row">
        <div class="form-group col-md-12">
            {!! Form::label('name', trans('locale.your_name')) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            {!! Form::label('email', trans('locale.email')) !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            {!! Form::label('phone_lb', trans('locale.phone')) !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('message_lb', trans('locale.message')) !!}
        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 text-center">
            <input type="file" name="file" id="file" class="inputfile" />
            <label id="inputLabel" class="btn bg-lifull text-white-color px-3" for="file">
                <i class="fas fa-upload"></i>
                <span class="file_name">File not selected</span>
            </label>
        </div>
        <div class="form-group col-md-6 text-center">
            {!! Form::submit(trans('locale.send_us_your_cv'), ['class' => 'btn bg-lifull text-white-color']) !!}
        </div>
    </div>
    {!! Form::hidden('job_position', $jobPosition) !!}
    {{ csrf_field() }}
{!! Form::close() !!}
<script type="text/javascript">
    var applyModal = $('#applyModal');
    @if (count($errors) > 0 || Session::has('status'))
        applyModal.modal('show');
    @endif

    // reset form when closing the modal
    applyModal.on('hidden.bs.modal', function (e) {
        $('#applyForm').find('input[type!=submit], textarea').val('');
        $('.file_name').html('File not selected');
    });

    setTimeout(function() {
        $('.alert-success').fadeOut();
    }, 5000);

    // custom file input label
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName ) {
                if (fileName.length > 15)
                    fileName = fileName.substr(0, 15);
                label.querySelector( '.file_name' ).innerHTML = fileName + '...';
            }
            else
                label.innerHTML = labelVal;
        });
    });
</script>