<h3>Candidate Info</h3>
<p>Name: {{ $candidate_name }}</p>
<p>Email: {{ $candidate_email }}</p>
<p>Phone: {{ $candidate_phone }}</p>
<p>Message: {{ $candidate_message }}</p>