<div class="container-fluid p-0 py-5">
    <div class="container text-left">
        <div class="row" id="recruitContent">
            <div class="col-md-12">
                <h2 class="section_title text-lifull-color text-center">
                    {{ (app()->getLocale() === 'jp') ? $article->jp_title : $article->title }}
                </h2>
                <hr/>
                @if (app()->getLocale() === 'jp')
                    {!! $article->jp_content !!}
                @else
                    {!! $article->content !!}
                @endif
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn bg-lifull text-white-color px-3" href="mailto:{{ $companyInfo['email'] }}" target="_blank">
                    <i class="fa fa-envelope"></i>
                    <span class="file_name">@lang('locale.send_us_your_cv')</span>
                </a>
            </div>
        </div>
    </div>
</div>
