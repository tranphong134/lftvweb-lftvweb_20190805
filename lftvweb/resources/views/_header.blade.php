<div class="container-fluid text-center subpage-header p-0 pt-5 pb-5"
    style="background-image:url({{ env('APP_URL') }}/storage/{{ $bannerImage }})">
    <h1 class='text-lifull-color h focusRing'>
        <span class="focusRing__ring">
            <span class="focusRing__ring">
            </span>
        </span>
        {{$pageName}}
    </h1>
</div>