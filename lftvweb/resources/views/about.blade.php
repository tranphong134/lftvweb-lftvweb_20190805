@extends('_layout')

@section('title', 'LFTV::Home')

@section('content')
    @include('_header', array('pageName' => 'About LFTV', 'bg_class' => 'lftv-about'))
    @include('abouts.about')
    @include('abouts.strongpoint')
    @include('abouts.service')
    @include('abouts.company')
@endsection