<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($articles as $article)
        <url>
            <loc>{{ env('APP_URL') }}/article/{{ $article->id }}</loc>
        </url>
    @endforeach
</urlset>