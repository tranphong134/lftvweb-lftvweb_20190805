<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($categories as $category)
        <url>
            <loc>{{ env('APP_URL') }}/category/{{ $category->id }}</loc>
        </url>
    @endforeach
</urlset>