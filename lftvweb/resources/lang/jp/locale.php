<?php

return [
    /**
     * Banner
     */
    'vision' => 'まだ手付かずの問題でも、視点を変える発想で豊かさに変わっていくはず。<br>そしてあらゆる人が、当たり前に無限の可能性の中から自分の生きたいLIFEを実現できる社会へ。',
    
    /**
     * Section title & Navigation item name
     */
    'about_us' => 'JP - About Us',
    'recruits' => 'JP - Recruits',
    'gallery' => 'JP - Gallery',
    'our_services' => 'JP - Services',

    /**
     * Button
     */
    'send_us_your_cv' => 'JP - Send us your CV',

    /**
     * Footer
     */
    'phone' => 'JP - Tel',
    'change_language_to' => 'JP - Change language to:',

    /**
     * Apply CV Form
     */
    'your_name' => 'JP - Your Name',
    'email' => 'JP - Email',
    'phone' => 'JP - Phone',
    'message' => 'JP - Message',
];
