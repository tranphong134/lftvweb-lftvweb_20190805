<?php

return [
    /**
     * Banner
     */
    'vision' => 'Be the world\'s most PASSIONATE company that provides TRUST, keeps KAIZEN<br> and creates the next STANDARD for tomorrow\'s SMILE :)',

    /**
     * Section title & Navigation item name
     */
    'about_us' => 'ABOUT US',
    'recruits' => 'RECRUITS',
    'gallery' => 'GALLERY',
    'our_services' => 'Services',

    /**
     * Button
     */
    'send_us_your_cv' => 'Send us your CV',

    /**
     * Footer
     */
    'telephone' => 'Tel',
    'change_language_to' => 'Change language to:',

    /**
     * Apply CV Form
     */
    'your_name' => 'Your Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'message' => 'Message'
];
