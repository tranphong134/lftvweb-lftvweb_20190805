<?php

define('DISABLED', 0);
define('ENABLED', 1);
define('STATUS', [
    DISABLED => 'Disabled',
    ENABLED => 'Enabled'
]);

/****CATEGORY****/
define('NEWS', 1);
define('RECRUIT', 2);
define('CONTACT', 7);

/****ABOUT ARTICLE****/
define('ABOUT_US', 7);
define('OUR_VISION', 21);
define('GUIDELINES', 25);
define('OFFSHORE_SERVICE', 11);
define('LAB_SERVICE', 12);
define('OUR_SERVICE', 24);

/****CONTACT FORM****/
define('SENT_SUCCESS_MESSAGE', '<h4>Message sent !</h4> <p>Thank you very much. Our team will reply soon.<p>');
define('GRID_STATUS', [
    ENABLED => '<span class="badge label-success">Yes</span>',
    DISABLED => '<span class="badge label-danger">No</span>'
]);

?>