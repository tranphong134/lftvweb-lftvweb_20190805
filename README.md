# lftvweb

## system overview
- Alpine Linux
- PHP 7.3
- MySQL 8.0
- Nginx
- Laravel 5.5

## Requirement
- composer
- node & npm

## Installation
```
$ git clone git@github.com:lifulltechvn/lftvweb.git
$ cd lftvweb
$ cp .env.example .env
$ npm install && npm rebuild node-sass && npm run development
$ composer install

$ docker-compose build
$ docker-compose up -d
```

access: http://0.0.0.0

